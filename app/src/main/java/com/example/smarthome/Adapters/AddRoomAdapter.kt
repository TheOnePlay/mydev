package com.example.smarthome.Adapters

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.smarthome.Classes.Room
import com.example.smarthome.R
import com.google.android.material.card.MaterialCardView

class AddRoomAdapter(val context: Context, val arrayRooms: ArrayList<Room>): RecyclerView.Adapter<AddRoomAdapter.ViewHolder>() {
    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val cardImage: ImageView = itemView.findViewById(R.id.cardImage)
        val cardTitle: TextView = itemView.findViewById(R.id.cardTitle)
        val roomCard:MaterialCardView = itemView.findViewById(R.id.roomCard)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_add_room,parent,false)
        return ViewHolder(view)
    }
    var lastCardView: MaterialCardView? = null
    var lastTextView: TextView? = null
    var flag = false
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.cardImage.setImageResource(arrayRooms[position].img)
        holder.cardTitle.text = arrayRooms[position].title

        holder.roomCard.setOnClickListener {
            holder.roomCard.isSelected = !holder.roomCard.isSelected
            if (holder.roomCard.isSelected) {
                if (flag) {
                    lastCardView?.isSelected = false
                    lastCardView?.setCardBackgroundColor(Color.parseColor("#FFFFFF"))
                    lastTextView?.isSelected = false
                }
                holder.roomCard.setCardBackgroundColor(Color.parseColor("#984E4F"))
                holder.cardTitle.isSelected = true
            } else {
                holder.roomCard.setCardBackgroundColor(Color.parseColor("#FFFFFF"))
                holder.cardTitle.isSelected = false
            }
            lastCardView = holder.roomCard
            lastTextView = holder.cardTitle
            flag = true
        }
    }
    override fun getItemCount() = arrayRooms.size
}