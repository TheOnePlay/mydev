package com.example.smarthome.Classes

import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import kotlin.experimental.and

class Hasher {
    fun getSha256Hash(password: String): String? {
        return try {
            var digest: MessageDigest? = null
            try {
                digest = MessageDigest.getInstance("SHA-256")
            } catch (e1: NoSuchAlgorithmException) {
                e1.printStackTrace()
            }
            digest!!.reset()
            bin2hex(digest.digest(password.toByteArray()))
        } catch (ignored: java.lang.Exception) {
            null
        }
    }
    fun bin2hex(data: ByteArray): String? {
        val hex = StringBuilder(data.size * 2)
        for (b in data) hex.append(String.format("%02x", b and 0xFF.toByte()))
        return hex.toString()
    }
}