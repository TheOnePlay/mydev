package com.example.smarthome

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.smarthome.Classes.AppInfo
import com.example.smarthome.Classes.Hasher
import com.example.smarthome.databinding.ActivityEnterBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.json.JSONObject
import java.net.URL
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import javax.net.ssl.HttpsURLConnection
import kotlin.experimental.and

class EnterActivity : AppCompatActivity() {
    lateinit var binding:ActivityEnterBinding
    val linkRegMob = "https://smarthome.madskill.ru/mobile"
    val linkEnter = "https://smarthome.madskill.ru/user"
    lateinit var keyDevice:String
    private lateinit var editor:SharedPreferences.Editor
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEnterBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        val uuid:String = sharedPreferences.getString("PHONE_UUID","nichego").toString()
        keyDevice = sharedPreferences.getString("KEY_DEVICE","null").toString()
        println(uuid)
        if (keyDevice == "null"){
            regMobile(uuid)
        }
        else{
            checkActual(uuid)
        }
        binding.apply {
            btnReg.setOnClickListener{
                val intent = Intent(applicationContext,RegActivity::class.java)
                startActivity(intent)
            }
            btnEnter.setOnClickListener{
                // проверка полей на пустоту
                val email:String = etMail.text.toString()
                var check = false
                for (i in email.indices){
                    if(email[i].toString() == "." || email[i].toString() == "@" || email[i].isDigit())
                        email[i].lowercase()
                    else {
                        if (email[i].toString() == email[i].toString().uppercase()){
                            check = true
                            break;
                        }
                    }
                }
                if (check){
                    val builder = AlertDialog.Builder(this@EnterActivity)
                    builder.setTitle("Attention")
                        .setMessage("Email может состоять только из маленьких букв и цифр")
                        .setPositiveButton("Ok"){
                            dialog, _ -> dialog.cancel()
                        }
                        .show()
                }
                else{
                    if (isValidEmail(etMail.text.toString()) && etPass.text.toString() != "") {
                        toEnter(uuid)
                        println("Key: $keyDevice")
                    }
                    else{
                        val builder = AlertDialog.Builder(this@EnterActivity)
                        builder.setTitle("Attention")
                            .setMessage("Некорректный ввод")
                            .setPositiveButton("Ok"){
                                    dialog, _ -> dialog.cancel()
                            }
                            .show()
                    }
                }
            }
        }
    }
    fun isValidEmail(target: CharSequence?): Boolean {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }
    private fun toEnter(uuid:String){
        CoroutineScope(Dispatchers.IO).launch {
            try {
                var url = URL(linkEnter)
                with (url.openConnection() as HttpsURLConnection){
                    setRequestProperty("X-HTTP-Method-Override", "OPTIONS")
                    requestMethod = "POST"
                    binding.apply {
                        setRequestProperty("email",etMail.text.toString())
                        setRequestProperty("password",etPass.text.toString())
                        setRequestProperty("uuid",uuid)
                        var hash:String = etMail.text.toString()+etPass.text.toString()+keyDevice
                        setRequestProperty("hash", Hasher().getSha256Hash(hash))
                        println(Hasher().getSha256Hash(hash))
                    }
                    var stringBuffer:StringBuffer = StringBuffer()
                    if(responseCode == 201){
                        inputStream.bufferedReader().use {
                            it.readLine().forEach { line ->
                                stringBuffer.append(line)
                            }
                        }
                        val jsonObject = JSONObject(stringBuffer.toString())
                        println(jsonObject)
                        println("Token = "+jsonObject.get("token"))
                        val token = jsonObject.get("token").toString()
                        withContext(Dispatchers.Main){
                            Toast.makeText(applicationContext,responseMessage, Toast.LENGTH_SHORT).show()
                            val intent = Intent(applicationContext,MainActivity::class.java)
                            intent.putExtra("TOKEN",token)
                            startActivity(intent)
                            finish()
                        }
                    }
                    else{
                        withContext(Dispatchers.Main){
                            val builder = AlertDialog.Builder(this@EnterActivity)
                            builder.setTitle("Attention")
                                .setMessage(responseMessage)
                                .setPositiveButton("Ok"){
                                        dialog,id -> dialog.cancel()
                                }
                            builder.show()
                        }
                    }
                    this.disconnect()
                }
            }catch (e: Exception){
                e.printStackTrace()
            }
        }
    }

    private fun regMobile(uuid:String){
        CoroutineScope(Dispatchers.IO).launch {
            try {
                var url = URL(linkRegMob)
                with (url.openConnection() as HttpsURLConnection){
                    requestMethod = "POST"

                    val data = "uuid=$uuid&appId=${AppInfo().appId}&device=${AppInfo().device}"
                    outputStream.bufferedWriter().use {
                        it.write(data)
                        it.flush()
                        it.close()
                    }
                    println("Response code1: $responseCode")
                    val stringBuffer = StringBuffer()
                    inputStream.bufferedReader().use {
                        it.readLine().forEach { line ->
                            stringBuffer.append(line)
                        }
                    }
                    editor.putString("KEY_DEVICE",JSONObject(stringBuffer.toString()).get("keyDevice").toString()).apply()
                    withContext(Dispatchers.Main){
                        keyDevice = JSONObject(stringBuffer.toString()).get("keyDevice").toString()
                    }
                    this.disconnect()
                }
            }catch (e:Exception){
                e.printStackTrace()
            }
        }
    }

    private fun checkActual(uuid: String) {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                var url = URL("$linkRegMob?uuid=$uuid")
                with (url.openConnection() as HttpsURLConnection){
                    setRequestProperty("X-HTTP-Method-Override","PATCH")
                    requestMethod = "POST"

                    println("Response code2: $responseCode")
                    println(responseMessage)

                    if (responseCode == 200) {
                        val stringBuffer = StringBuffer()
                        inputStream.bufferedReader().use {
                            it.readLine().forEach { line ->
                                stringBuffer.append(line)
                            }
                        }
                        editor.putString("KEY_DEVICE",JSONObject(stringBuffer.toString()).get("keyDevice").toString()).apply()
                        withContext(Dispatchers.Main){
                            keyDevice = JSONObject(stringBuffer.toString()).get("keyDevice").toString()
                            Toast.makeText(applicationContext,"KeyDevice updating",Toast.LENGTH_SHORT).show()
                        }
                    }
                    this.disconnect()
                }
            }catch (e:Exception){
                e.printStackTrace()
            }
        }
    }

}