package com.example.smarthome

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat.animate
import com.example.smarthome.Classes.AppInfo
import com.example.smarthome.databinding.ActivityLaunchBinding
import kotlinx.coroutines.*
import java.net.URL
import java.util.*
import javax.net.ssl.*

class LaunchActivity : AppCompatActivity() {
    lateinit var binding: ActivityLaunchBinding
    lateinit var editor: SharedPreferences.Editor
    val linkRegApp = "https://smarthome.madskill.ru/app"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLaunchBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // создаётся SharedPreferences
        val sharedPreferences = getSharedPreferences("sharedPrefs",Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        binding.apply {
                    // настройка анимации
                    animate(imgLogo).apply{
                        rotationBy(360f)
                        duration = 6000L
                        withStartAction {
                            if (sharedPreferences.getString("APP_REG","false").toString() == "false"){
                                regApp()
                            }
                            // по началу анимации сохраняется UUID устройства
                            if(sharedPreferences.getString("PHONE_UUID","nichego").toString() == "nichego"){
                                editor.apply{
                                    putString("PHONE_UUID", UUID.randomUUID().toString().toUpperCase())
                                }.apply()
                            }

                        }
                        withEndAction{
                            // по окончанию анимации переход на экран входа
                            val intent = Intent(applicationContext,EnterActivity::class.java)
                            startActivity(intent)
                            finish()
                        }
                        start()
                    }
        }
    }
    private fun regApp() {
        CoroutineScope(Dispatchers.IO).launch {
            val url = URL(linkRegApp)
            with (url.openConnection() as HttpsURLConnection){
                requestMethod = "POST"

                val info = "appId=${AppInfo().appId}&competitor=${AppInfo().competitor}"

                outputStream.bufferedWriter().use{
                    it.write(info)
                    it.flush()
                    it.close()
                }
                if (responseCode == 201) {
                    withContext(Dispatchers.Main){
                        editor.putString("APP_REG","true").apply()
                    }
                }
            }
        }
    }
}

